import { hasPath, pipe, indexBy, prop, mapObjIndexed, path } from 'ramda'
import { Store } from 'redux'
import axios, { AxiosInstance, AxiosPromise, AxiosResponse, AxiosError, AxiosRequestConfig } from 'axios'
import { RootState } from './rootReducer'

import * as Auth from './services/Auth'

export type AxiosRequestPayload = any
export type AxiosRequestParams = any
export interface AxiosResponsePayload<T = {}> {
  response: T
}

export type Response<T> = AxiosPromise<T> | Promise<T>

type AxiosPath = string

interface HttpParams {
  baseURL?: string
  store?: Store<RootState>
}

interface HttpClientInterface {
  service: AxiosInstance

  config(params: HttpParams): void

  handleResponseSuccess(response: AxiosResponse<AxiosResponsePayload>): AxiosResponse<AxiosResponsePayload>
  handleResponseError(error: AxiosError): AxiosPromise

  get(path: AxiosPath, params?: AxiosRequestParams): AxiosPromise<AxiosResponsePayload>
  patch(path: AxiosPath, payload?: AxiosRequestConfig): AxiosPromise<AxiosResponsePayload>
  post(path: AxiosPath, payload?: AxiosRequestConfig): AxiosPromise<AxiosResponsePayload>
  put(path: AxiosPath, payload?: AxiosRequestConfig): AxiosPromise<AxiosResponsePayload>
  delete(path: AxiosPath): AxiosPromise<AxiosResponsePayload>
}

export function makeResponse<T, U extends { data: any }>(mock: T, request: () => Promise<U>) {
  if (process.env.REACT_APP_USE_MOCK === 'true') {
    // tslint:disable-next-line: no-console
    console.log('Mocked response:', mock)
    return Promise.resolve(mock)
  }

  return request().then(res => res.data)
}

export class HttpClientService implements HttpClientInterface {
  public service: AxiosInstance
  private store?: Store<RootState>

  constructor() {
    const service = axios.create()

    service.interceptors.request.use(this.handleRequest)
    service.interceptors.response.use(this.handleResponseSuccess, this.handleResponseError)

    this.service = service
  }

  public config(params: HttpParams) {
    if (params.baseURL) {
      this.service.defaults.baseURL = params.baseURL
    }

    if (params.store) {
      this.store = params.store
    }
  }

  public handleRequest = (request: AxiosRequestConfig) => {
    if (this.store) {
      const state = this.store.getState()
      const token = state.auth.tokens.accessToken
      if (token.length) {
        request.headers.common.Authorization = token
      }
    }

    return request
  }

  public handleResponseSuccess = (
    response: AxiosResponse<AxiosResponsePayload>
  ): AxiosResponse<AxiosResponsePayload> => {
    return response
  }

  public handleResponseError = (error: AxiosError): AxiosPromise => {
    const { response } = error

    if (
      this.store &&
      error.config.url &&
      error.config.url.includes(Auth.service.urls.refreshToken) &&
      response &&
      [401].includes(response.status)
    ) {
      this.store.dispatch(Auth.actions.logoutAsync.success({ success: true }))
      localStorage.removeItem('accessToken')
      localStorage.removeItem('refreshToken')
    } else if (this.store && response && response.status === 401) {
      const store = this.store
      const refreshToken = store.getState().auth.tokens.refreshToken

      if (refreshToken.length) {
        const data: Auth.types.RefreshTokenRequestData = { refreshToken }

        return this.post(Auth.service.urls.refreshToken, data).then(
          (newResponse: AxiosResponse<Auth.types.RefreshTokenResponseSuccessData>) => {
            store.dispatch(Auth.actions.refreshTokenAsync.success(newResponse.data))
            error.config.headers.Authorization = `Bearer ${newResponse.data.accessToken}`
            return Promise.resolve(axios(error.config)) // TODO: May be no Promise.resolve wrap?
          }
        )
      }

      return Promise.reject(error)
    } else if (response && response.status === 400 && this.isValidationError(response.data)) {
      response.data = this.validationNormalizer(response.data)
    }

    return Promise.reject(error)
  }

  public get(url: string, params?: AxiosRequestParams) {
    return this.service.request({
      url,
      params,
      method: 'GET',
      responseType: 'json'
    })
  }

  public patch(url: AxiosPath, data?: AxiosRequestPayload) {
    return this.service.request({
      url,
      data,
      method: 'PATCH',
      responseType: 'json'
    })
  }

  public post(url: AxiosPath, data?: AxiosRequestPayload) {
    return this.service.request({
      url,
      data,
      method: 'POST',
      responseType: 'json'
    })
  }

  public put(url: AxiosPath, data?: AxiosRequestPayload) {
    return this.service.request({
      url,
      data,
      method: 'PUT',
      responseType: 'json'
    })
  }

  public delete(url: AxiosPath) {
    return this.service.request({
      url,
      method: 'DELETE',
      responseType: 'json'
    })
  }

  private validationNormalizer(data: any) {
    return pipe(
      indexBy((obj: { [k: string]: any }) => prop('propertyPath', obj)),
      mapObjIndexed(path(['message']))
    )(data)
  }

  private isValidationError(data: any) {
    return hasPath(['0', 'message'], data) && hasPath(['0', 'propertyPath'], data)
  }
}

export const httpClient = new HttpClientService()
