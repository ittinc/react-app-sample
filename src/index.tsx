/// <reference types="webpack-env" />

import React, { ComponentType } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { I18nextProvider } from 'react-i18next'

import 'bootstrap/dist/css/bootstrap.css'
import '@fortawesome/fontawesome-free/css/all.css'
import './index.css'
import i18n from './i18n'
import App from './App'
import store from './store'
import { history } from './rootReducer'

if (!process.env.REACT_APP_USE_MOCK) {
  process.env.REACT_APP_USE_MOCK = 'false'
}

if (process.env.REACT_APP_USE_MOCK === 'false' && !process.env.REACT_APP_API_ENDPOINT) {
  throw new Error('You must provide env.REACT_APP_API_ENDPOINT')
}

const render = (Component: ComponentType) => {
  return ReactDOM.render(
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Component />
        </ConnectedRouter>
      </Provider>
    </I18nextProvider>,
    document.getElementById('root')
  )
}

render(App)

if (module.hot && process.env.NODE_ENV !== 'production') {
  module.hot.accept('./App', () => {
    const nextApp = require('./App').default
    render(nextApp)
  })
}
