export const reHydrateStore = () => {
  try {
    return JSON.parse(localStorage.getItem('applicationState') || '{}')
  } catch (error) {
    return {}
  }
}
