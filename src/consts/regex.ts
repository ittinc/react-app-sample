export const phoneRegex = /^\+(?:[0-9] ?){6,14}[0-9]$/
export const loginRegex = /^(\+(?:[0-9] ?){6,14}[0-9])|(^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+)$/
