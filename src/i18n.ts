import { match, identity } from 'ramda'
import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

const normalizeLocale = (locale: string): string => match(/(.*)-(.*)/, locale)[1]

const languageDetector: i18n.LanguageDetectorModule = {
  type: 'languageDetector',
  detect() {
    return normalizeLocale(navigator.language)
  },
  init: identity,
  cacheUserLanguage: locale => localStorage.setItem('i18n', locale)
}

i18n
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    detection: {
      order: ['localStorage', 'navigator'],
      lookupLocalStorage: 'i18n'
    },
    react: {
      useSuspense: false
    },
    load: 'languageOnly',
    fallbackLng: 'en',
    lowerCaseLng: true,
    debug: process.env.NODE_ENV !== 'production',
    interpolation: {
      escapeValue: false // React escapes by themself
    },
    resources: {
      en: {
        translation: require('./locales/en/tranlations.json')
      },
      ru: {
        translation: require('./locales/ru/tranlations.json')
      }
    }
  })

export default i18n

export const changeLocale = (locale: string) => i18n.changeLanguage(locale)
