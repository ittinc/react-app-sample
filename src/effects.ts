import { call, delay } from 'redux-saga/effects'

type ms = number

export function* repeatEvery(duration: ms, fn: (this: void, ...args: any[]) => void, ...args: any[]) {
  while (true) {
    yield call(fn, ...args)
    yield delay(duration)
  }
}
