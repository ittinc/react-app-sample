import { trim } from 'ramda'
import { Store, createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import { routerMiddleware } from 'connected-react-router'
import decode from 'jwt-decode'

import { state, RootState, history } from './rootReducer'
import { sagaManager } from './rootSaga'
import { reHydrateStore } from './rootState'
import { httpClient } from './HttpClient'
import * as Auth from './services/Auth'

const isTokenValid = (token: string): boolean => {
  if (process.env.REACT_APP_USE_MOCK === 'true') {
    return true
  }

  try {
    const decoded: { exp: number } = decode(token)
    return decoded.exp > Date.now() / 1000
  } catch (error) {
    return false
  }
}

const sagaMiddleware = createSagaMiddleware()
const middleware = [routerMiddleware(history), sagaMiddleware]

const composeEnhancers =
  process.env.NODE_ENV !== 'production' ? composeWithDevTools({ trace: true, traceLimit: 25 }) : compose
const enhancer = composeEnhancers(applyMiddleware(...middleware))

const configureStore = () => {
  const store: Store<RootState> = createStore(state, reHydrateStore(), enhancer)

  const accessToken: string = localStorage.getItem('accessToken') || ''
  const refreshToken: string = localStorage.getItem('refreshToken') || ''

  if (isTokenValid(refreshToken)) {
    store.dispatch(Auth.actions.loginAsync.success({ accessToken, refreshToken }))
  }

  httpClient.config({ store, baseURL: trim(process.env.REACT_APP_API_ENDPOINT || '') })

  sagaManager.startSagas(sagaMiddleware)

  if (module.hot && process.env.NODE_ENV !== 'production') {
    module.hot.accept('./rootReducer', () => {
      store.replaceReducer(require('./rootReducer'))
    })

    module.hot.accept('./rootSaga', () => {
      sagaManager.cancelSagas(store)
      require('./rootSaga').sagaManager.startSagas(sagaMiddleware)
    })
  }

  return store
}

export const executeSaga = sagaManager.executeSaga(sagaMiddleware)
export default configureStore()
