import React from 'react'
import { Route } from 'react-router-dom'
import * as Router from 'react-router'

interface Props {
  title: string
  setTitle?: () => string
}

const CustomLayout: React.StatelessComponent<Props> = props => {
  const title = props.setTitle ? props.setTitle() : ''
  document.title = title && title.length ? title : props.title

  return <div>{props.children}</div>
}

interface ComponentProps {
  setTitle?: (x: Router.RouteComponentProps<any>) => () => string
}

interface RouteProps extends Router.RouteProps {
  component: (React.ComponentClass<any, any> | React.FunctionComponent<any>) & ComponentProps
  title: string
}

interface RouteState {}

class LayoutRoute extends React.PureComponent<RouteProps, RouteState> {
  public readonly state: RouteState = {}

  public render() {
    const { component: Component, title, ...rest } = this.props

    const render = (matchProps: Router.RouteComponentProps) => (
      <CustomLayout title={title} setTitle={Component.setTitle ? Component.setTitle(matchProps) : undefined}>
        <Component {...matchProps} />
      </CustomLayout>
    )

    return <Route {...rest} render={render} />
  }
}

export default LayoutRoute
