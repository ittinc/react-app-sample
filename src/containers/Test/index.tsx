import * as React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { connect } from 'react-redux'
import { withTranslation, WithTranslation } from 'react-i18next'

import { RootState } from '../../rootReducer'
import { Header } from '../../components/Header'

interface OwnProps {}

interface OwnState {}

type Props = ReturnType<typeof mapStateToProps> &
  typeof mapDispatchToProps &
  RouteComponentProps &
  WithTranslation &
  OwnProps

class TestContainer extends React.Component<Props, OwnState> {
  constructor(props: Props) {
    super(props)

    this.state = {}
  }

  public componentDidMount() {}

  public render(): React.ReactElement<{}> {
    return (
      <>
        <Header />
        Test
      </>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  user: state.auth.user
})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation()(TestContainer))
