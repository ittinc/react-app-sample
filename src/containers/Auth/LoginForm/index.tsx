import * as React from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Card, Button, Form, Row, Col } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import * as Yup from 'yup'
import { withFormik, FormikProps, Form as FormikForm, Field } from 'formik'

import { CustomCard } from '../../../components/CustomCard'
import { CenteredWrapper } from '../../../components/CenteredWrapper'
import { renderField } from '../../utils'

import { loginRegex } from '../../../consts/regex'
import { executeSaga } from '../../../store'
import * as Auth from '../../../services/Auth'

interface OwnProps {}
interface OwnState {}

type Props = OwnProps & FormikProps<Auth.types.LoginRequestData> & WithTranslation

class LoginFormComponent extends React.PureComponent<Props, OwnState> {
  public render(): React.ReactElement<{}> {
    const { isSubmitting, isValid, error, errors, touched, t } = this.props

    return (
      <>
        <CenteredWrapper>
          <CustomCard>
            <Card.Body>
              <NavLink to="/auth/register">
                <Button variant="outline-primary" className="float-right">
                  {t('auth.login.button_registration')}
                </Button>
              </NavLink>
              <Card.Title className="mb-4 mt-1">{t('auth.login.head_title')}</Card.Title>
              <p>
                <Button variant="outline-info" block={true} href="#">
                  <i className="fab fa-google" />
                  &nbsp; Login via Google
                </Button>
                <Button variant="outline-primary" block={true} href="#">
                  <i className="fab fa-linkedin" />
                  &nbsp; Login via LinkedIn
                </Button>
              </p>
              <hr />
              <FormikForm>
                <Form.Group>
                  <Field name="login" render={renderField({ placeholder: 'Email or phone', type: 'text' })} />
                </Form.Group>
                <Form.Group>
                  <Field name="password" render={renderField({ placeholder: 'Password', type: 'password' })} />
                </Form.Group>
                <Row>
                  <Col md={6}>
                    <Form.Group>
                      <Button variant="primary" block={true} type="submit" disabled={!isValid && isSubmitting}>
                        Login
                      </Button>
                    </Form.Group>
                  </Col>
                  <Col md={6} className="text-right">
                    <NavLink to="" className="small">
                      Forgot password?
                    </NavLink>
                  </Col>
                </Row>
              </FormikForm>
            </Card.Body>
          </CustomCard>
        </CenteredWrapper>
      </>
    )
  }
}

const LoginForm = withFormik<OwnProps, Auth.types.LoginRequestData>({
  mapPropsToValues: props => ({
    login: '',
    password: ''
  }),
  validationSchema: Yup.object().shape({
    login: Yup.string()
      .matches(loginRegex)
      .required(),
    password: Yup.string()
      .min(8)
      .matches(/^\S+$/)
      .required()
  }),
  handleSubmit: (values, actions) => {
    executeSaga(Auth.sagas.loginUser, values, actions)
  }
})(withTranslation()(LoginFormComponent))

export default LoginForm
