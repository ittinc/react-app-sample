import * as React from 'react'
import { withTranslation, WithTranslation } from 'react-i18next'
import { Card, Button, Form } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import * as Yup from 'yup'
import { withFormik, FormikProps, Form as FormikForm, Field, FieldProps } from 'formik'

import { CustomCard } from '../../../components/CustomCard'
import { CenteredWrapper } from '../../../components/CenteredWrapper'
import { renderField } from '../../utils'

import { phoneRegex } from '../../../consts/regex'
import { executeSaga } from '../../../store'
import * as Auth from '../../../services/Auth'

interface OwnProps {}
interface OwnState {}

type Props = OwnProps & FormikProps<Auth.types.RegisterRequestData> & WithTranslation

class RegisterFormComponent extends React.PureComponent<Props, OwnState> {
  public render(): React.ReactElement<{}> {
    const { isSubmitting, isValid, error, errors, touched, t } = this.props

    return (
      <>
        <CenteredWrapper>
          <CustomCard>
            <Card.Body>
              <NavLink to="/auth/login">
                <Button variant="outline-primary" className="float-right">
                  Sign in
                </Button>
              </NavLink>
              <Card.Title className="mb-4 mt-1">Sign up</Card.Title>
              <p>
                <Button variant="outline-info" block={true} href="#">
                  <i className="fab fa-twitter" />
                  &nbsp; Register via Twitter
                </Button>
                <Button variant="outline-primary" block={true} href="#">
                  <i className="fab fa-facebook-f" />
                  &nbsp; Register via Facebook
                </Button>
              </p>
              <hr />
              <FormikForm>
                <Form.Group>
                  <Field name="email" render={renderField({ placeholder: 'Email*', type: 'email' })} />
                </Form.Group>
                <Form.Group>
                  <Field name="phone" render={renderField({ placeholder: 'Phone', type: 'text' })} />
                </Form.Group>
                <Form.Group>
                  <Field name="firstName" render={renderField({ placeholder: 'First name*', type: 'text' })} />
                </Form.Group>
                <Form.Group>
                  <Field name="lastName" render={renderField({ placeholder: 'Last name', type: 'text' })} />
                </Form.Group>
                <Form.Group>
                  <Field name="password" render={renderField({ placeholder: 'Password*', type: 'password' })} />
                </Form.Group>
                <Form.Group>
                  <Field name="passwordRepeat" render={renderField({ placeholder: 'Password repeat*', type: 'password' })} />
                </Form.Group>
                <Form.Group>
                  <Button variant="primary" block={true} type="submit" disabled={!isValid && isSubmitting}>
                    Register
                  </Button>
                </Form.Group>
                <p className="small text-secondary">Fields marked with "*" are required</p>
              </FormikForm>
            </Card.Body>
          </CustomCard>
        </CenteredWrapper>
      </>
    )
  }
}

const RegisterForm = withFormik<OwnProps, Auth.types.RegisterRequestData>({
  mapPropsToValues: props => ({
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    password: '',
    passwordRepeat: ''
  }),
  validationSchema: Yup.object().shape({
    email: Yup.string()
      .email()
      .required(),
    phone: Yup.string().matches(phoneRegex),
    password: Yup.string()
      .min(8)
      .matches(/^\S+$/)
      .required(),
    passwordRepeat: Yup.string().oneOf([Yup.ref('password'), null], 'Passwords must match')
  }),
  handleSubmit: (values, actions) => {
    executeSaga(Auth.sagas.registerUser, values, actions)
  }
})(withTranslation()(RegisterFormComponent))

export default RegisterForm
