import * as React from 'react'
import { RouteComponentProps } from 'react-router-dom'

import LoginForm from '../LoginForm'

interface OwnProps {}
interface OwnState {}
interface UrlParams {}

// Sample
type Props = RouteComponentProps<UrlParams> & OwnProps

class Login extends React.PureComponent<Props, OwnState> {
  public render(): React.ReactElement<{}> {
    return <LoginForm />
  }
}

export default Login
