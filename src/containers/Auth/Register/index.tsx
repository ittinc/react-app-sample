import * as React from 'react'

import RegisterForm from '../RegisterForm'

interface OwnProps {}
interface OwnState {}

type Props = OwnProps

class Register extends React.PureComponent<Props, OwnState> {
  public render(): React.ReactElement<{}> {
    return <RegisterForm />
  }
}

export default Register
