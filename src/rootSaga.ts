import { Store } from 'redux'
import { Saga, SagaMiddleware } from 'redux-saga'
import { all, take, fork, cancel } from 'redux-saga/effects'

import * as Auth from './services/Auth'

export default function* rootSaga() {
  // Here must be forked all services
  yield all([fork(Auth.rootSaga)])
}

const sagas = [rootSaga]

export const CANCEL_SAGAS_HMR = '@SAMPLE_APP/CANCEL_SAGAS_HMR'

function createAbortableSaga(saga: Saga) {
  if (process.env.NODE_ENV === 'development') {
    return function* main() {
      const sagaTask = yield fork(saga)

      yield take(CANCEL_SAGAS_HMR)
      yield cancel(sagaTask)
    }
  }

  return saga
}

export const sagaManager = {
  startSagas(sagaMiddleware: SagaMiddleware) {
    sagas.map(createAbortableSaga).forEach(saga => sagaMiddleware.run(saga))
  },
  cancelSagas(store: Store) {
    store.dispatch({ type: CANCEL_SAGAS_HMR })
  },
  executeSaga(sagaMiddleware: SagaMiddleware) {
    return (saga: Saga, ...args: any[]) => sagaMiddleware.run(saga, ...args).toPromise()
  }
}
