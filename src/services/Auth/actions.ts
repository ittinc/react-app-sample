import { createAsyncAction } from 'typesafe-actions'

import * as Auth from '.'

/* prettier-ignore */
////////////////////////////////////////////// Register
export const registerAsync = createAsyncAction(
  '@SAMPLE/AUTH/REGISTER_REQUEST',
  '@SAMPLE/AUTH/REGISTER_REQUEST_SUCCESS',
  '@SAMPLE/AUTH/REGISTER_REQUEST_ERROR'
)<Auth.types.RegisterRequestData, Auth.types.RegisterResponseSuccessData, Auth.types.RegisterResponseErrorData>();

/* prettier-ignore */
////////////////////////////////////////////// Login
export const loginAsync = createAsyncAction(
  '@SAMPLE/AUTH/LOGIN_REQUEST',
  '@SAMPLE/AUTH/LOGIN_REQUEST_SUCCESS',
  '@SAMPLE/AUTH/LOGIN_REQUEST_ERROR'
)<Auth.types.LoginRequestData, Auth.types.LoginResponseSuccessData, Auth.types.LoginResponseErrorData>();

/* prettier-ignore */
////////////////////////////////////////////// Refresh token
export const refreshTokenAsync = createAsyncAction(
  '@SAMPLE/AUTH/REFRESH_TOKEN_REQUEST',
  '@SAMPLE/AUTH/REFRESH_TOKEN_REQUEST_SUCCESS',
  '@SAMPLE/AUTH/REFRESH_TOKEN_REQUEST_ERROR'
)<Auth.types.RefreshTokenRequestData, Auth.types.RefreshTokenResponseSuccessData, Auth.types.RefreshTokenResponseErrorData>();

/* prettier-ignore */
////////////////////////////////////////////// Logout
export const logoutAsync = createAsyncAction(
  '@SAMPLE/AUTH/LOGOUT_REQUEST',
  '@SAMPLE/AUTH/LOGOUT_REQUEST_SUCCESS',
  '@SAMPLE/AUTH/LOGOUT_REQUEST_ERROR'
)<undefined, Auth.types.LogoutResponseSuccessData, Auth.types.LogoutResponseErrorData>();

/* prettier-ignore */
////////////////////////////////////////////// User info
export const userInfoAsync = createAsyncAction(
  '@SAMPLE/AUTH/USER_INFO_REQUEST',
  '@SAMPLE/AUTH/USER_INFO_REQUEST_SUCCESS',
  '@SAMPLE/AUTH/USER_INFO_REQUEST_ERROR'
)<undefined, Auth.types.UserInfoResponseSuccessData, Auth.types.UserInfoResponseErrorData>();
