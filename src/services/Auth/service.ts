import { httpClient, makeResponse, Response } from '../../HttpClient'
import * as Auth from '.'

export const urls = {
  register: '/api/auth/register',
  login: '/api/auth/login',
  refreshToken: '/api/auth/relogin',
  userInfo: '/api/auth/me',
  logout: '/api/auth/logout'
  // test(queryparams): `/api/auth/logout/${queryparams.someParam}`,
}

// prettier-ignore
export const api = {
  register(data: Auth.types.RegisterRequestData): Response<Auth.types.RegisterResponseSuccessData> {
    return makeResponse(Auth.mocks.register, () => httpClient.post(urls.register, data))
  },
  login(data: Auth.types.LoginRequestData): Response<Auth.types.LoginResponseSuccessData> {
    return makeResponse(Auth.mocks.login, () => httpClient.post(urls.login, data))
  },
  refreshToken(data: Auth.types.RefreshTokenRequestData): Response<Auth.types.RefreshTokenResponseSuccessData> {
    return makeResponse(Auth.mocks.refreshToken, () => httpClient.post(urls.refreshToken, data))
  },
  userInfo(): Response<Auth.types.UserInfoResponseSuccessData> {
    return makeResponse(Auth.mocks.userData, () => httpClient.get(urls.userInfo))
  },
  logout(data: Auth.types.LogoutRequestData): Response<Auth.types.LogoutResponseSuccessData> {
    return makeResponse(Auth.mocks.logout, () => httpClient.post(urls.logout, data))
  }
  // test(data: Auth.types.Test, params: SomeType): Response<Auth.types.TestResponseSuccessData> {
  //   return makeResponse(Auth.mocks.logout, () => httpClient.post(urls.test(params), data))
  // },
}
