export interface State {
  tokens: Tokens
  user: User
  authorized: boolean
}

export interface User {
  id: number
  firstName: string
  lastName: string | null
  email: string
  phone: string | null
}

export interface Tokens {
  accessToken: string
  refreshToken: string
}

export * from './api'
