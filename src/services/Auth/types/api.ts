import * as service from './service'

////////////////////////////////////////////// Login

export interface LoginRequestData {
  login: string
  password: string
}

export type LoginResponseSuccessData = service.Tokens
export interface LoginResponseErrorData {}

////////////////////////////////////////////// Refresh token

export type RefreshTokenRequestData = Pick<service.Tokens, 'refreshToken'>
export type RefreshTokenResponseSuccessData = service.Tokens
export interface RefreshTokenResponseErrorData {}

////////////////////////////////////////////// Register

export interface RegisterRequestData {
  firstName: string
  lastName?: string
  phone?: string
  email: string
  password: string
}

export interface RegisterResponseSuccessData {
  user: service.User
  tokens: service.Tokens
}

export interface RegisterResponseErrorData {}

////////////////////////////////////////////// Logout

export type LogoutRequestData = Pick<service.Tokens, 'refreshToken'>
export interface LogoutResponseSuccessData {
  success: boolean
}
export interface LogoutResponseErrorData {}

////////////////////////////////////////////// User info

export type UserInfoResponseSuccessData = service.User
export interface UserInfoResponseErrorData {}
