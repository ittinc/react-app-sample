import { always, T, assoc, evolve, Evolver } from 'ramda'
import { createReducer, ActionType } from 'typesafe-actions'

import * as Auth from '.'

export const initialState: Auth.types.State = {
  tokens: {
    accessToken: '',
    refreshToken: ''
  },
  authorized: false,
  user: {
    id: 0,
    firstName: '',
    lastName: '',
    email: '',
    phone: ''
  }
}

/* prettier-ignore */
export const rootReducer = createReducer<Auth.types.State, ActionType<typeof Auth.actions>>(initialState)
  .handleAction([Auth.actions.refreshTokenAsync.success, Auth.actions.loginAsync.success], (state, action) => {
    return evolve({
        tokens: always(action.payload),
        authorized: T
      } as Evolver,
      state
    )
  })
  .handleAction(Auth.actions.logoutAsync.success, (state, action) => initialState)
  .handleAction(Auth.actions.userInfoAsync.success, (state, action) => assoc('user', action.payload, state))
