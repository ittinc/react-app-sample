import * as R from 'ramda'
import { all, call, put, takeLatest, select } from 'redux-saga/effects'
import { push } from 'connected-react-router'
import { FormikActions, FormikBag } from 'formik'
import { getType } from 'typesafe-actions'

import * as Auth from '.'
import { RootState } from '../../rootReducer'

////////////////////////////////////////////// Handlers

function* fetchUserInfo() {
  try {
    const userInfoResponse = yield call(Auth.service.api.userInfo)
    yield put(Auth.actions.userInfoAsync.success(userInfoResponse))
  } catch (error) {}
}

function* authCommon(loginResponse: Auth.types.LoginResponseSuccessData, formikActions: FormikActions<{}>) {
  yield put(Auth.actions.loginAsync.success(loginResponse))
  formikActions.setSubmitting(false)

  const tokens = { accessToken: loginResponse.accessToken, refreshToken: loginResponse.refreshToken }
  yield updateTokens({ payload: tokens })

  yield fetchUserInfo()
  yield put(push('/'))
}

function* updateTokens(action: { payload: Auth.types.RefreshTokenResponseSuccessData }) {
  yield localStorage.setItem('accessToken', action.payload.accessToken)
  yield localStorage.setItem('refreshToken', action.payload.refreshToken)
}

export function* loginUser(
  payload: Auth.types.LoginRequestData,
  formikActions: FormikBag<{}, Auth.types.LoginRequestData>
) {
  try {
    const data: Auth.types.LoginRequestData = payload
    formikActions.setSubmitting(true)
    const response = yield call(Auth.service.api.login, data)
    yield authCommon(response, formikActions)
  } catch (error) {
    formikActions.setSubmitting(false)
    formikActions.setError(error)
  }
}

export function* registerUser(
  payload: Auth.types.RegisterRequestData,
  formikActions: FormikBag<{}, Auth.types.RegisterRequestData>
) {
  try {
    const clearedPayload = R.reject(R.isEmpty, payload) as Auth.types.RegisterRequestData
    formikActions.setSubmitting(true)
    const response = yield call(Auth.service.api.register, clearedPayload)
    const tokens = response.tokens
    yield put(Auth.actions.registerAsync.request(response))
    yield authCommon(tokens, formikActions)
  } catch (error) {
    formikActions.setSubmitting(false)
    formikActions.setError(error)
  }
}

function* logoutUser() {
  try {
    const refreshToken = yield select((state: RootState) => state.auth.tokens.refreshToken)
    localStorage.removeItem('accessToken')
    localStorage.removeItem('refreshToken')

    const response = yield call(Auth.service.api.logout, { refreshToken })
    yield put(Auth.actions.loginAsync.success(response))
  } catch (error) {
    // TODO: handle error
  }
}

////////////////////////////////////////////// Root saga

export function* rootSaga() {
  /* prettier-ignore */
  yield all([
    fetchUserInfo(),
    takeLatest<any>(getType(Auth.actions.logoutAsync.request), logoutUser),
    takeLatest<any>(getType(Auth.actions.refreshTokenAsync.success), updateTokens),
    takeLatest<any>(getType(Auth.actions.userInfoAsync.request), fetchUserInfo)
  ])
}
