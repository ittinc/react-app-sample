import * as React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { withTranslation, WithTranslation } from 'react-i18next'

import { RootState } from './rootReducer'

import Main from './containers/Main'
import Test from './containers/Test'

import Login from './containers/Auth/Login'
import Register from './containers/Auth/Register'

import CustomLayout from './Layouts/CustomLayout'

interface OwnProps {}
interface OwnState {}

type RouteMapProps = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps & OwnProps & WithTranslation

class RouteMapComponent extends React.Component<RouteMapProps, OwnState> {
  public render(): React.ReactElement<{}> {
    return (
      <>
        {this.redirects()}
        {this.routes()}
      </>
    )
  }

  private routes(): React.ReactElement<{}> {
    const { t } = this.props
    return (
      <>
        <CustomLayout exact={true} path="/" component={Main} title={t('main.head_title')} />
        <CustomLayout exact={true} path="/test" component={Test} title={t('main.head_title')} />

        <CustomLayout exact={true} path="/auth/login" component={Login} title={t('auth.login.head_title')} />
        <CustomLayout exact={true} path="/auth/register" component={Register} title={t('auth.register.head_title')} />
      </>
    )
  }

  private redirects(): React.ReactElement<{}> | null {
    if (this.props.authorized && this.props.user) {
      if (this.props.user.id !== 0) {
        return this.props.location.pathname.includes('auth') ? <Redirect to="/" /> : null
      }

      return null
    }

    return !this.props.location.pathname.includes('auth') ? <Redirect to="/auth/login" /> : null
  }
}

const mapStateToProps = (state: RootState) => ({
  authorized: state.auth.authorized,
  user: state.auth.user,
  location: state.router.location
})

const mapDispatchToProps = {}

export const RouteMap = connect(
  mapStateToProps,
  mapDispatchToProps
)(withTranslation()(RouteMapComponent))
