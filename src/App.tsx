import { connect } from 'react-redux'
import React from 'react'

import { RootState } from './rootReducer'
import { RouteMap } from './routes'

interface OwnProps {}

type Props = OwnProps & typeof mapDispatchToProps & ReturnType<typeof mapStateToProps>

interface OwnState {}

class App extends React.Component<Props, OwnState> {
  public render() {
    return (
      <div id="main">
        <RouteMap />
      </div>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  authorized: state.auth.authorized
})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
