import { combineReducers } from 'redux'
import { createBrowserHistory } from 'history'
import { connectRouter } from 'connected-react-router'
import { StateType } from 'typesafe-actions'

import * as Auth from './services/Auth'

export const history = createBrowserHistory()

export const state = combineReducers({
  router: connectRouter(history),
  auth: Auth.rootReducer
})

export type RootState = StateType<typeof state>
