import React from 'react'
import { Modal, Button } from 'react-bootstrap'

interface ModalProps {
  heading: string
  show: boolean
  onHide(): void
}

export const CustomModal = (props: React.PropsWithChildren<ModalProps>) => {
  return (
    <Modal size="lg" aria-labelledby="contained-modal-title-vcenter" centered={true} {...props}>
      <Modal.Header closeButton={true}>
        <Modal.Title>{props.heading}</Modal.Title>
      </Modal.Header>
      <Modal.Body>{props.children}</Modal.Body>
      <Modal.Footer>
        <Button onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  )
}
