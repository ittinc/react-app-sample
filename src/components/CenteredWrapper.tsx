import styled from 'styled-components/macro'

export const CenteredWrapper = styled.div`
  display: flex;
  align-items: center;
  min-height: 100vh;
  justify-content: center;
`
