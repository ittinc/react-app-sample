import React from 'react'
import styled from 'styled-components/macro'

import { withTranslation, WithTranslation } from 'react-i18next'

const Container = styled.div``

interface OwnProps {
  isAlert: boolean
}

type Props = OwnProps & WithTranslation

const BackupYellowButton: React.StatelessComponent<Props> = (props: Props) => {
  return <Container>Button</Container>
}

export default withTranslation()(BackupYellowButton)
